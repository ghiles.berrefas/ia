from typing import Tuple
import tensorflow as tf

class Param_to_modify():
  optimizers: tf.keras.optimizers  
  learning_rate: int  # compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3)
  regularization: tf.keras.regularizers
  activation_function: str = "relu"
  epoch: int = 25
  repartition_data : list = []
  def __init__(self, 
              optimizers: tf.keras.optimizers = None ,
              learning_rate = 0.001, 
              activation_function = None,
              epoch: int = 25,
              repartition_data : list = [],
              regularization: tf.keras.regularizers = None,  
              ) -> None:
    print("#######################")
    self.optimizers = optimizers 
    self.learning_rate = learning_rate 
    self.activation_function = activation_function
    self.epoch = epoch 
    self.repartition_data = repartition_data
    print(self.repartition_data)
    self.regularization = regularization
    pass

# def get_methods(object, spacing=20):
#   methodList = []
#   for method_name in dir(object):
#     try:
#         if callable(getattr(object, method_name)):
#             methodList.append(str(method_name))
#     except Exception:
#         methodList.append(str(method_name))
#   return list(filter(lambda elem : not elem.startswith("_"), methodList)) 